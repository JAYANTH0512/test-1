package com.innominds;
/**
 * This is the main method to run the program  
 */

public class Main {
	
		public static void main(String[] args) {           // Main Method
			Answer sol=new Answer();           			   // object creation
			String output=sol.decodeString("3[a]2[bc]");
			String output1=sol.decodeString( "3[a2[c]]");
			String output2=sol.decodeString( "2[abc]3[cd]ef");
			System.out.println(output);
			System.out.println(output1);
			System.out.println(output2);
		}

}
