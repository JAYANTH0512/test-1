package design;

import java.util.ArrayList;
import java.util.List;

public class Main1 {
	{
		try {

			List<Integer> list = new ArrayList();
			list.add(10);
			System.out.println("Entering" + "try Statment");
			Integer a = list.get(1);
			System.out.println("Accesing the first element: " + a);

		} catch (IndexOutOfBoundsException e) {
			System.out.println("CaughtIndexOutOfBoundsException :" + e.getMessage());

		} finally {
			System.out.println("This will be executed");
		}
	}
}
